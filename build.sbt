name := "lolparser"

version := "0.1"

scalaVersion := "2.12.6"

libraryDependencies ++= Seq(
  "org.dispatchhttp" %% "dispatch-core" % "0.14.0",
  "com.typesafe.play" %% "play-json" % "2.6.10"
)