import play.api.libs.json.{JsArray, JsObject, JsValue}
import dispatch._

import scala.concurrent.{ExecutionContext, ExecutionContextExecutor}
import scala.util.{Failure, Success}

class MapsMaker {
  implicit val ec: ExecutionContextExecutor = ExecutionContext.global
  private val util = new Util

  private def itemsMapFormer(version: String): Future[Map[String, String]] = {
    util.request(util.itemsAPI(version)).map(data => Map("0" -> "None") ++
      (data \ "data").as[JsObject].keys.filter { id: String => (data \ "data" \ id \ "maps" \ "11").as[Boolean] }
      .flatMap(id => Map(id -> (data \ "data" \ id \ "name").as[String])).toMap[String, String])
  }

  private def championsMapFormer(version: String): Future[Map[String, String]] = {
    util.request(util.championsAPI(version)).map(data => (data \ "data").as[JsObject].keys
      .map(name => (data \ "data" \ name \ "key").as[String] -> name).toMap[String, String])
  }

  private def runesMapFormer(version: String): Future[Map[String, String]] = {
    util.request(util.runesAPI(version)).map(data =>
    data.as[JsArray].value.flatMap(branch => {
      (branch \ "slots").as[JsArray].value.flatMap(slot => (slot \ "runes").as[JsArray].value
        .flatMap(rune => Map((rune \ "id").as[JsValue].toString ->
          (rune \ "key").as[JsValue].toString.replaceAll("\"", "")))).toMap[String, String]
    }).toMap[String, String])
  }

  private def mapFormerHelper(path: String, version: String, method: String => Future[Map[String, String]]): Map[String, String] = {
    val source = util.fileGetOrNew(path)

    if (source.isEmpty) {
      val temp = method(version)
      temp.onComplete {
        case Success(value) => util.printWriter(path, value.keys.map(elem => s"$elem _ ${value(elem)}\n").toSet.foldRight("")(_ + _), append = false)
        case Failure(err) => println(s"Error while forming Map: $err")
      }

      temp.apply()
    }
    else {
      source.split("\n").flatMap(string => {
        val temp = string.split(" _ ")
        Map(temp(0) -> temp(1))
      }).toMap[String, String]
    }
  }

  def idMapFormer(version: String): Map[String, Map[String, String]] = {
    Map("items"     -> mapFormerHelper(util.idMapsPath(version) + "items.csv", version, itemsMapFormer),
      "champions" -> mapFormerHelper(util.idMapsPath(version) + "champions.csv", version, championsMapFormer),
      "runes"     -> mapFormerHelper(util.idMapsPath(version) + "runes.csv", version, runesMapFormer))
  }
}
