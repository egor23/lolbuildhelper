package models

import play.api.libs.json.Json

case class ParticipantPlayer(summonerId: Long)

object ParticipantPlayer {
  implicit val j_ParticipantPlayer = Json.format[ParticipantPlayer]
}
