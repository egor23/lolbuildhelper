package models

import play.api.libs.json.Json

case class MatchDetails(gameVersion: String, participantIdentities: Seq[Participant])

object MatchDetails {
  implicit val j_MatchDetails = Json.format[MatchDetails]
}
