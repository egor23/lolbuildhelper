package models

import play.api.libs.json.Json

case class Participant(participantId: Int, player: ParticipantPlayer)

object Participant {
  implicit val j_Participant = Json.format[Participant]
}
