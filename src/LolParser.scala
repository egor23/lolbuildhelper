import java.util.concurrent.atomic.AtomicReference

import models.SummonerId

import scala.collection.immutable.ListMap
import scala.concurrent.{ExecutionContext, ExecutionContextExecutor, Future, Promise}
import scala.util.{Failure, Success, Try}

object LolParser extends App {
  val apiClient = new RiotGamesApiClient
  val util = new Util
  implicit val ec: ExecutionContextExecutor = ExecutionContext.global

  implicit class FutureExtensions[T](f: Future[T]) {
    def mapAll[Target](m: Try[T] => Target)(implicit ec: ExecutionContext): Future[Target] = mapAllEx(x => Success(m(x)))

    def mapAllEx[Target](m: Try[T] => Try[Target])(implicit ec: ExecutionContext): Future[Target] = {
      val promise = Promise[Target]()
      f.onComplete { r =>
        try {
          promise complete m(r)
        } catch {
          case err: Throwable => promise failure err
        }
      }(ec)
      promise.future
    }

    def flatMapAll[Target](m: Try[T] => Future[Target])(implicit ec: ExecutionContext): Future[Target] = {
      val promise = Promise[Target]()
      f.onComplete { r =>
        try {
          m(r).onComplete { z => promise complete z }(ec)
        } catch {
          case err: Throwable => promise failure err
        }
      }(ec)
      promise.future
    }
  }

  class AsyncSequentializer(implicit ec: ExecutionContext) {
    private val tail = new AtomicReference[Future[_]](Future.successful(()))

    def nextAction[T](action: => Future[T]): Future[T] = {
      // create next promise
      val promise = Promise[T]()

      // get current tail and set the new one
      val prev = tail.getAndSet(promise.future)

      // attach an action to the previous tail future
      prev flatMapAll { case _ =>
        action
      } onComplete promise.complete

      promise.future
    }
  }

  def matchesDataFileFormer(summoner: SummonerId, matches: IndexedSeq[String], version: String): Future[Unit] = {
    println(s"Forming matches data file for $summoner and $matches")
    val path = util.matchListFile(summoner.id, summoner.server, version)
    val matchListSource = util.fileGetOrNew(path)

    val matchList = if (matchListSource.isEmpty){
      util.printWriter(path, matches.map(id => s"$id\n").toSet.foldRight("")(_ + _), append = true)
      matches
    }
    else{
      val matchListDiff = (matches diff matchListSource.split("\n")).toList
      if (matchListDiff.nonEmpty) {
        util.printWriter(path, matches.map(id => s"$id\n").toSet.foldRight("")(_ + _), append = true)
      }
      matchListDiff
    }
    val sequentializer = new AsyncSequentializer()

    Future.sequence(matchList.map(m => sequentializer.nextAction(loadAndSaveMatchData(summoner, m, version)))) map (_ => ())
  }

  private def loadAndSaveMatchData(summoner: SummonerId, matchId: String, version: String): Future[Unit] = {
    Thread.sleep(50)
    val f = apiClient.getMatchData(summoner, matchId, version)
    f onComplete {
      case Success(matchData) =>
        if (matchData.isDefined) {
          if (matchData.get("version") != "wrong") {
            val string = matchData.get.keys.map(key => s"$key _ ${matchData.get(key)}; ").toSet.foldRight("")(_ + _)
            util.printWriter(util.matchesDataFile(summoner.id, summoner.server, version), string + "\n", append = true)
          }
        }
      case Failure(err) => println(s"Error while writing match data: $err")
    }

    f.map(_ => ())
  }

    def championStatisticFormer(summoner: SummonerId, version: String): Future[Unit] = Future {
      println(s"Forming champion stats data file for $summoner")
    val path = util.statisticsFile(summoner.name, summoner.server, version)
    val statisticSource = util.fileGetOrNew(util.matchesDataFile(summoner.id, summoner.server, version))

    if (statisticSource.nonEmpty) {
      val temp = statisticSource.split("\n").map(string => string.split("; ")
        .flatMap(elem => Map(elem.split(" _ ")(0) -> elem.split(" _ ")(1))).toMap[String, String])

      val champToWins = temp.map(map => map("championId") -> map("win")).toList.groupBy(_._1)
        .flatMap(elem => Map(elem._1 -> elem._2.map(x => x._2)))

      val champToKda = temp.map(map => map("championId") -> map("kda")).toList.groupBy(_._1)
        .flatMap(elem => Map(elem._1 -> elem._2.map(x => x._2)))

      val champsList = temp.map(map => map("championId")).toList

      val champToPickRate = ListMap(champsList.groupBy(identity).mapValues(elem => elem.size).toSeq.sortWith(_._2 > _._2): _*)
        .flatMap(elem => Map(elem._1 -> s"${elem._2}/${champsList.length} (${util.fractionToString(elem._2, champsList.length)}%)"))

      val champToWinrate = champToWins.mapValues(elem => s"${elem.count(_ == "true")}/${elem.length} (${util.fractionToString(elem.count(_ == "true"), elem.length)}%)")

      val champsStats = champToPickRate.flatMap(elem => Map(elem._1 -> s"${elem._2} _ ${champToWinrate(elem._1)}"))

      val champToAverageKda = champToKda.mapValues(elem => elem.map(kda => kda.split('/').map(x => x.toInt).toList)
        .transpose.map(_.sum).map(temp => util.fractionToString(temp, elem.length * 100) + "/").foldRight("")(_ + _).dropRight(1))

      val string = s"Champion${util.spaces(12)}Pick Rate${util.spaces(16)}Win Rate${util.spaces(17)}Avarage K/D/A\n\n" + champsStats.map(elem => {
        val temp = elem._2.split(" _ ")
        s"${elem._1 + util.spaces(20, elem._1) + temp(0) + util.spaces(25, temp(0)) + temp(1) + util.spaces(25, temp(1)) + champToAverageKda(elem._1)}\n"
      }).foldRight("")(_ + _)

      util.printWriter(path, string, append = false)
    }
    else{
      util.printWriter(path, "No games this patch", append = false)
    }
  }

  def processor(summonerName: String, server: String, version: String): Unit = {
    val f = for {
      summoner <- apiClient.getSummonerId(summonerName, server)
      matchList <- apiClient.getMatchList(summoner)
      _ <- matchesDataFileFormer(summoner, matchList, version)
      _ <- championStatisticFormer(summoner, version)
    } yield {
      println("Ended.")
    }

    f.onComplete {
      case Success(_) => println("Finished successfully")
      case Failure(err) => println(s"Finished with errors $err")
    }
  }

  processor("aphromoo", "na1", apiClient.version)

  //apiClient.getMatchData2(SummonerId("name", "id" ,"euw1"), 3747118153L, "v")
//  apiClient.getSummonerId("DieDoktorarbyte", "euw1") onComplete {
//    case Success(id) =>
//      println(s"Got summoner id $id")
//    case Failure(err) =>
//      println(s"Unable to get summoner id due to error $err")
//  }

  Console.readLine()
}


