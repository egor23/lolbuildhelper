import java.io.{File, FileWriter, PrintWriter}
import java.nio.file.Paths

import dispatch.{Future, Http, Res, url}
import play.api.libs.json.{JsValue, Json}

import scala.concurrent.ExecutionContext.Implicits.global
import scala.io.Source
import scala.util.{Failure, Success}

class Util() {

  def riotApi(server: String): String = s"https://$server.api.riotgames.com"
  val summonerApi: String = "/lol/summoner/v3/summoners/by-name/"
  val matchApi: String = "/lol/match/v3/matches/"
  val matchListApi: String = "/lol/match/v3/matchlists/by-account/"

  val versionApi = "https://ddragon.leagueoflegends.com/api/versions.json"
  def itemsAPI(version: String): String = s"http://ddragon.leagueoflegends.com/cdn/$version/data/en_US/item.json"
  def runesAPI(version: String): String = s"http://ddragon.leagueoflegends.com/cdn/$version/data/en_US/runesReforged.json"
  def championsAPI(version: String): String = s"http://ddragon.leagueoflegends.com/cdn/$version/data/en_US/champion.json"

  val apiKey: String = "RGAPI-f6443171-c365-43c0-92c4-686b3f8a8a50"

  val runesFile = s"${Paths.get("").toAbsolutePath}\\info\\runes.csv"
  val itemsFile = s"${Paths.get("").toAbsolutePath}\\info\\items.csv"
  val championsFile = s"${Paths.get("").toAbsolutePath}\\info\\champions.csv"
  def matchListFile(accountId: String, server: String, version: String) = s"${Paths.get("").toAbsolutePath}\\info\\$version\\matchList\\$server\\$accountId\\matchId's.csv"
  val summonerToIdFile = s"${Paths.get("").toAbsolutePath}\\info\\summonerToId.csv"
  def idMapsPath(version: String) = s"${Paths.get("").toAbsolutePath}\\info\\$version\\"
  def matchesDataFile(accountId: String, server: String, version: String) = s"${Paths.get("").toAbsolutePath}\\info\\$version\\matchList\\$server\\$accountId\\matchData's.csv"
  def statisticsFile(summonerName: String, server: String, version: String) = s"${Paths.get("").toAbsolutePath}\\statistics\\$version\\$server\\$summonerName.csv"

  def request(ref: String): Future[JsValue] = {
    val res_F: Future[Res] = Http.default(url(ref))

    res_F.onComplete {
      case Success(_) =>
      case Failure(err) => println(s"Error: $err")
    }

    res_F map { res =>
      Json.parse(res.getResponseBody)
    }
  }

  def fileGetOrNew(path: String): String = {
    try{
      Source.fromFile(path)
    } catch {
      case e: java.io.IOException =>
        new File(path.split('\\').dropRight(1).mkString("\\")).mkdirs()
        new File(path).createNewFile()
      case e: java.io.FileNotFoundException => new File(path).createNewFile()
      case e: Exception => println(s"Got error: $e")
    }

    Source.fromFile(path).mkString
  }

  def printWriter(path: String, data: String, append: Boolean): Unit = {
    try {
       new FileWriter(path, append)
    }
    catch {
      case e: java.io.IOException =>
        new File(path.split('\\').dropRight(1).mkString("\\")).mkdirs()
        new File(path).createNewFile()
      case e: java.io.FileNotFoundException => new File(path).createNewFile()
      case e: Exception => println(s"Got error: $e")
    }

    val pw = new PrintWriter(new FileWriter(path, append))
    pw.append(data)
    pw.close()
  }

  def fractionToString(numerator: Int, denominator: Int): String = {
    val result = s"${((numerator.toDouble / denominator * 100) * 10).round / 10.toDouble}"
    if (result.split('.')(1) == "0") s"${result.dropRight(2)}"
    else result
  }

  def spaces(quantity: Int, string: String = ""): String = {
    (for (_ <- 0 to quantity - string.length) yield " ").foldRight("")(_ + _)
  }
}
