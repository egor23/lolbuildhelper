import java.util.concurrent.atomic.AtomicReference

import com.sun.net.httpserver.Authenticator
import models.{MatchDetails, SummonerId}
import play.api.libs.json.{JsArray, JsValue}

import scala.concurrent.{ExecutionContext, ExecutionContextExecutor, Future, Promise}
import scala.util.{Failure, Success, Try}

class RiotGamesApiClient {
  implicit val ec: ExecutionContextExecutor = ExecutionContext.global
  private val util = new Util
  private val maper = new MapsMaker

//  val version = {
//    util.request(util.versionApi).onComplete {
//      case Success(value) => value.as[JsArray].value(0).toString.replaceAll("\"", "")
//      case Failure(err) => println(s"")
//    }
//  }
  val version = "8.18.2"

  val idMap: Map[String, Map[String, String]] = maper.idMapFormer(version)

  private def requestFormerHelper(api: String, param: String, server: String): String = {
    util.riotApi(server) + api + param + "?api_key=" + util.apiKey
  }

  private def requestFormer(api: String, param: Any, server: String): String = api match {
    case "summoner"  => requestFormerHelper(util.summonerApi, param.toString, server)
    case "matchList" => requestFormerHelper(util.matchListApi, param.toString, server)
    case "match"     => requestFormerHelper(util.matchApi, param.toString, server)
  }

  private def getSummonerIdRequest(summonerName: String, server: String): Future[SummonerId] = {
    util.request(requestFormer("summoner", summonerName.replace(" ", "%20"), server))
      .map(elem => SummonerId(summonerName, elem.\("accountId").as[JsValue].toString, server))
  }

  def getSummonerId(summonerName: String, server: String): Future[SummonerId] = {
    println(s"Getting summoner id for $summonerName:$server")
    getSummonerIdFromCacheOrLoad(summonerName, server, loadSummonerId)

    getSummonerIdFromCache(summonerName, server) flatMap {
      case Some(id) => Future.successful(id)
      case None =>
        loadSummonerId(summonerName, server) mapAll {
          case Success(id) =>
            saveSummonerIdToCache(id)
            id
          case Failure(err) =>
            println(s"Failed to get summoner id due to error $err")
            throw err
        }
    }
  }

  private def getSummonerIdFromCache(summonerName: String, server: String): Future[Option[SummonerId]] = {
    val data = util.fileGetOrNew(util.summonerToIdFile).split("\n").map(string => string.split(" _ ")).find(elem => elem(0) == summonerName)
    Future.successful(data.map(d => SummonerId(summonerName, d(1), d(2))))
  }

  private def loadSummonerId(summonerName: String, server: String): Future[SummonerId] = {
    util.request(requestFormer("summoner", summonerName.replace(" ", "%20"), server))
      .map(elem => SummonerId(summonerName, elem.\("accountId").as[JsValue].toString, server))
  }

  private def saveSummonerIdToCache(summoner: SummonerId): Unit = {
    val summonerId =  s"${summoner.name} _ ${summoner.id} _ ${summoner.server}\n"
    util.printWriter(util.summonerToIdFile, summonerId, append = true)
  }

  private def getSummonerIdFromCacheOrLoad(summonerName: String, server: String, loadFunc: (String, String) => Future[SummonerId]): Future[SummonerId] = {
    getSummonerIdFromCache(summonerName, server) flatMap {
      case Some(id) => Future.successful(id)
      case None =>
        val id = loadFunc(summonerName, server)

        id.onComplete {
          case Success(xxx) => saveSummonerIdToCache(xxx)
        }

        id
    }
  }

  def getMatchList(summonerId: SummonerId): Future[IndexedSeq[String]] = {
    println(s"Getting match list for $summonerId")
    util.request(requestFormer("matchList", summonerId.id, summonerId.server))
      .map(elem => (elem \ "matches").as[JsArray].value.map(game => (game \ "gameId").as[JsValue].toString()))
  }

  def getMatchList2(summonerId: SummonerId): Future[IndexedSeq[String]] = withRequest[IndexedSeq[String]](requestFormer("matchList", summonerId.id, summonerId.server)) { js =>
    (js \ "matches").as[JsArray].value.map(game => (game \ "gameId").as[JsValue].toString())
  }

  def getMatchData2(summonerId: SummonerId, matchId: Long, version: String): Future[Option[Map[String, String]]] = withRequest[Option[Map[String, String]]](requestFormer("match", matchId, summonerId.server)) { js =>
    val response = js.as[MatchDetails]

    println(response.gameVersion)

    // response.participantIdentities.foreach(println)
    val participantId: Option[Int] = response.participantIdentities.find(_.player.summonerId == summonerId.id).map(_.participantId)

    participantId match {
      case Some(id) => Some(Map())
      case None => None
    }
  }

  def getMatchData(summonerId: SummonerId, matchId: String, version: String): Future[Option[Map[String, String]]] = {
    util.request(requestFormer("match", matchId, summonerId.server)).map(elem => {
      if ((elem \ "gameVersion").asOpt[String].get.contains(version.dropRight(2)) && (elem \ "gameMode").as[String] == "CLASSIC") {
        val playerId = ((elem \ "participantIdentities").as[JsArray].value.find(elem => (elem \ "player" \ "accountId")
          .as[JsValue].toString == summonerId.id).get.as[JsValue] \ "participantId").as[JsValue].toString
        val playerData = (elem \ "participants").as[JsArray].value.find(elem => (elem \ "stats" \ "participantId").as[JsValue].toString == playerId).get.as[JsValue]
        val playerStats = (playerData \ "stats").as[JsValue]

        Some(Map("version" -> (elem \ "gameVersion").as[String].split('.').dropRight(2).mkString(".")) +
          ("win" -> (playerStats \ "win").as[JsValue].toString) + ("championId" -> idMap("champions")((playerData \ "championId").as[JsValue].toString)) +
          ("kda" -> s"${(playerStats \ "kills").as[JsValue].toString}/${(playerStats \ "deaths").as[JsValue].toString}/${(playerStats \ "assists").as[JsValue].toString}") ++
          (for (i <- 0 to 5) yield {
            Map("perk" + i -> idMap("runes")((playerStats \ ("perk" + i.toString)).as[JsValue].toString)) ++
              Map("item" + i -> idMap("items")((playerStats \ ("item" + i.toString)).as[JsValue].toString))
          }).flatten.toMap[String, String])
      }
      else if ((elem\ "gameMode").as[String] != "CLASSIC") {
        Some(Map("version" -> "wrong"))
      }
      else {
        None
      }
    })
  }

  private def withRequest[T](request: String)(action: JsValue => T): Future[T] = {
    util.request(request) map action
  }

  implicit class FutureExtensions[T](f: Future[T]) {
    def mapAll[Target](m: Try[T] => Target)(implicit ec: ExecutionContext): Future[Target] = mapAllEx(x => Success(m(x)))

    def mapAllEx[Target](m: Try[T] => Try[Target])(implicit ec: ExecutionContext): Future[Target] = {
      val promise = Promise[Target]()
      f.onComplete { r =>
        try {
          promise complete m(r)
        } catch {
          case err: Throwable => promise failure err
        }
      }(ec)
      promise.future
    }

    def flatMapAll[Target](m: Try[T] => Future[Target])(implicit ec: ExecutionContext): Future[Target] = {
      val promise = Promise[Target]()
      f.onComplete { r =>
        try {
          m(r).onComplete { z => promise complete z }(ec)
        } catch {
          case err: Throwable => promise failure err
        }
      }(ec)
      promise.future
    }
  }

  class AsyncSequentializer(implicit ec: ExecutionContext) {
    private val tail = new AtomicReference[Future[_]](Future.successful(()))

    def nextAction[T](action: => Future[T]): Future[T] = {
      // create next promise
      val promise = Promise[T]()

      // get current tail and set the new one
      val prev = tail.getAndSet(promise.future)

      // attach an action to the previous tail future
      prev flatMapAll { case _ =>
        action
      } onComplete promise.complete

      promise.future
    }
  }
}